import json

import aiohttp
import config
from fastapi import APIRouter
from fastapi.responses import Response
from routers import get_file_async

router = APIRouter()


@router.get("/api/dog")
async def get_dog() -> Response:
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        result: bytes = await get_file_async(client, config.DOG_URL)
        result_dict: dict = json.loads(result)
        url: str = result_dict.get("message")
        result = await get_file_async(client, url)
    return Response(content=result, media_type="bytes")

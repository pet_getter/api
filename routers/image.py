import aiohttp
from config import COLLECTION_LIMIT
from dals import ImageDAL, UserDAL, UserImageDAL
from database.db import async_session
from fastapi import APIRouter, status
from fastapi.responses import Response
from models import Image, User, UserImage
from routers import get_file_async
from schemas import ImageCreationResponse, ImageToSave

router = APIRouter(prefix="/api/image")


@router.post("/save", response_model=ImageCreationResponse)
async def save_images(data: ImageToSave, response: Response):
    success = False
    message = "Failed to save"
    response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        file_raw = await get_file_async(client, data.src_link)
        image_data = {"file_id": data.file_id, "file_raw": file_raw}

    async with async_session() as session:
        async with session.begin():
            user_dal = UserDAL(session)
            user: "User" = await user_dal.get_by_tg_id(data.user_id)

            if len(user.images) >= COLLECTION_LIMIT:
                message = "Collection limit reached!"
                success = True
                response.status_code = status.HTTP_429_TOO_MANY_REQUESTS
            else:
                image_dal = ImageDAL(session)
                image: "Image" = await image_dal.get_or_create(**image_data)

                user_image_dal = UserImageDAL(session)

                if await user_image_dal.get(user_id=user.id, image_id=image.id):
                    message = "File already in collection!"
                    success = True
                    response.status_code = status.HTTP_200_OK

                else:
                    user_image: "UserImage" = await user_image_dal.create(
                        **{"user_id": user.id, "image_id": image.id}
                    )
                    if user_image:
                        message = "File added to collection!"
                        success = True
                        response.status_code = status.HTTP_201_CREATED

    return {
        "success": success,
        "message": message,
    }

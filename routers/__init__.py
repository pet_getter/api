import logging

import aiohttp

logger = logging.getLogger(__name__)


async def get_file_async(client: "aiohttp.ClientSession", url: str) -> bytes:
    response: "aiohttp.ClientResponse" = await client.get(url)
    return await response.read()

import aiohttp
import config
from fastapi import APIRouter
from fastapi.responses import Response
from routers import get_file_async

router = APIRouter()


@router.get("/api/cat")
async def get_cat() -> Response:
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        result = await get_file_async(client, config.CAT_URL)
    return Response(content=result, media_type="bytes")

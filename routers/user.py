import logging
import typing
from datetime import datetime

from dals import UserDAL, UserImageDAL
from database.db import async_session
from fastapi import APIRouter
from fastapi.responses import Response, StreamingResponse
from models.user import User
from routers.utils import create_zip_file, iter_file
from schemas import ImageRemovedResponse, RemoveUserImage, UserIn, UserOut

router = APIRouter(prefix="/api/user")

logger = logging.getLogger(__name__)


@router.get("", response_model=list[UserOut])
async def get_users() -> list[User]:
    async with async_session() as session:
        async with session.begin():
            user_dal: "UserDAL" = UserDAL(session)
            return await user_dal.all()


@router.get("/{tg_id}", response_model=UserOut)
async def get_users(tg_id: int) -> User:
    async with async_session() as session:
        async with session.begin():
            user_dal: "UserDAL" = UserDAL(session)
            return await user_dal.get_by_tg_id(tg_id)


@router.post("/increase", response_model=UserOut)
async def increase_users(user_data: UserIn) -> User:
    async with async_session() as session:
        async with session.begin():
            user_dal: "UserDAL" = UserDAL(session)
            user: "User" = await user_dal.get_by_tg_id_or_create(**user_data.dict())
            return await user_dal.increase_requests(user.tg_id)


@router.get("/user_images/{tg_id}")
async def get_images(tg_id: int) -> Response:
    files: list = list()
    file_name = f"{tg_id}_{str(datetime.now())}.zip"
    async with async_session() as session:
        async with session.begin():
            user_dal: "UserDAL" = UserDAL(session)
            user: "typing.Optional[User]" = await user_dal.get_by_tg_id(tg_id)
            if user_images := user.images:
                files: list = [image.src for image in user_images]
    zipfile = create_zip_file(files, tg_id, file_name)
    return StreamingResponse(iter_file(zipfile), media_type="image/jpeg")


@router.post("/user_images/remove", response_model=ImageRemovedResponse)
async def remove_file_from_collection(data: RemoveUserImage):
    message = "Failed to remove"
    success = False
    try:
        async with async_session() as session:
            async with session.begin():
                user_image_dal = UserImageDAL(session)
                success = await user_image_dal.delete_from_api(**data.dict())
                if success:
                    message = "Successfully removed"
    except Exception as exc:
        logger.error(exc)
    return {
        "success": success,
        "message": message,
    }

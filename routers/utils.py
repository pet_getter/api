import os
import typing
import zipfile

from config import TEMP_FOLDER

zip_subdir = TEMP_FOLDER


def iter_file(filename: str) -> typing.Generator:
    with open(filename, "rb") as file:
        yield from file


def create_zip_file(
    files: list, user_id: int, zip_file_name: str = "archive.zip"
) -> str:
    zip_file_folder: str = os.path.join(TEMP_FOLDER, str(user_id))
    zip_file_path: str = os.path.join(zip_file_folder, zip_file_name)

    if not os.path.exists(zip_file_folder):
        os.makedirs(zip_file_folder)

    with zipfile.ZipFile(zip_file_path, "w") as zp:
        for file in files:
            filename = file.split(os.sep)[-1]
            zp.write(file, filename)
    return zip_file_path

import config
import sentry_sdk
import uvicorn
from app_factory import create_app
from database.db import async_session, engine, generate_db

app = create_app()


@app.on_event("startup")
async def startup() -> None:
    await generate_db()


@app.on_event("shutdown")
async def shutdown():
    async with async_session() as session:
        await session.close()
    await engine.dispose()


if sentry_token := config.SENTRY_URL:
    sentry_sdk.init(dsn=sentry_token, traces_sample_rate=1.0)

if __name__ == "__main__":
    uvicorn.run("main:app")

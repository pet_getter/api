import logging.config
import os
import typing

from dotenv import load_dotenv

load_dotenv()

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

DATABASE_NAME: typing.Optional[str] = os.environ.get("DB_NAME")
DATABASE_USER: typing.Optional[str] = os.environ.get("POSTGRES_USER")
DATABASE_USER_PASSWORD: typing.Optional[str] = os.environ.get("POSTGRES_PASSWORD")
DATABASE_PORT: typing.Optional[str] = os.environ.get("POSTGRES_PORT")
ADMIN_USER_ID: typing.Optional[int] = int(os.environ.get("ADMIN_ID"))

SENTRY_URL: str = os.environ.get("SENTRY_URL")

PET_MEDIA_ROOT: str = os.path.join(APP_ROOT, "media/images")

CAT_URL: str = "https://cataas.com/cat"
DOG_URL: str = "https://dog.ceo/api/breeds/image/random"

COLLECTION_LIMIT: int = 10

TEMP_FOLDER: str = os.path.join(APP_ROOT, "tmp")

if not os.path.exists(TEMP_FOLDER):
    os.makedirs(TEMP_FOLDER)

FORMAT: str = "%(levelname)s | %(name)s | %(asctime)s | %(lineno)d | %(message)s"
LOGGER_LEVEL: int = int(os.environ.get("LOGGER_LEVEL", 20))

dict_config: dict = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {"base": {"format": FORMAT}},
    "handlers": {
        "file": {
            "class": "logger.LevelFileHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
            "when": "d",
            "interval": 1,
        },
        "stream": {
            "class": "logging.StreamHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
        },
    },
    "loggers": {
        "": {
            "level": LOGGER_LEVEL,
            "handlers": ["file", "stream"],
            "propagate": False,
        }
    },
}
logging.config.dictConfig(dict_config)

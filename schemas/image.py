import pydantic


class ImageToSave(pydantic.BaseModel):
    """Image In class"""

    file_id: str
    src_link: str
    user_id: int


class ImageOut(pydantic.BaseModel):
    """Image Out class"""

    id: int

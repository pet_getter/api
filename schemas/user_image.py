import pydantic


class RemoveUserImage(pydantic.BaseModel):
    tg_id: int
    file_name: str

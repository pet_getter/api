import pydantic


class ImageCreationResponse(pydantic.BaseModel):
    success: bool
    message: str


class ImageRemovedResponse(ImageCreationResponse):
    ...

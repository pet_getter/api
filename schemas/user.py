import pydantic
from schemas import ImageOut


class UserIn(pydantic.BaseModel):
    tg_id: int = pydantic.Field(..., alias="id")
    first_name: str | None = None
    username: str | None = None
    last_name: str | None = None


class UserOut(pydantic.BaseModel):
    id: int
    tg_id: int
    first_name: str | None = None
    username: str | None = None
    last_name: str | None = None
    requests: int

    class Config:
        orm_mode = True


class UserWithImagesOut(UserOut):
    images: list[ImageOut]

    class Config:
        orm_mode = True

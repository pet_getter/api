import logging
import os
import typing

import aiofiles
from config import PET_MEDIA_ROOT
from models import Image
from sqlalchemy.future import select
from sqlalchemy.orm import Session
from sqlalchemy.sql import Select

logger = logging.getLogger(__name__)


class ImageDAL:
    def __init__(self, db_session: Session):
        self.session = db_session

    async def create(self, **data) -> "Image":
        new_image: "Image" = Image(**data)
        self.session.add(new_image)
        logger.info(f"new image: id {new_image} added!")
        return new_image

    async def get_by_file_id(self, file_id: str) -> "Image":
        query: "Select" = select(Image).where(Image.file_id == file_id)
        result = await self.session.execute(query)
        if result := result.one_or_none():
            (result,) = result
        return result

    async def save_file(self, file_raw: bytes, file_id: str) -> str:
        path = os.path.join(PET_MEDIA_ROOT, file_id)
        async with aiofiles.open(path, mode="wb") as f:
            await f.write(file_raw)
            result = await self.create(src=path, file_id=file_id)
        return result

    async def get_or_create(
        self, file_raw: bytes, file_id: str
    ) -> typing.Union["Image", str]:
        file: "Image" = await self.get_by_file_id(file_id)
        if file:
            return file
        else:
            return await self.save_file(file_raw, file_id)

import logging

from dals import ImageDAL, UserDAL
from models import UserImage
from sqlalchemy.future import select
from sqlalchemy.orm import Session
from sqlalchemy.sql import Select, delete

logger = logging.getLogger(__name__)


class UserImageDAL:
    def __init__(self, db_session: Session):
        self.session = db_session

    async def create(self, **data) -> "UserImage":
        instance: "UserImage" = UserImage(**data)
        self.session.add(instance)
        logger.info(
            f"new image: id {instance.user_id}, image id: {instance.image_id} added!"
        )
        return instance

    async def get(self, user_id: int, image_id: int) -> "UserImage":
        query: "Select" = select(UserImage).where(
            UserImage.user_id == user_id, UserImage.image_id == image_id
        )
        result = await self.session.execute(query)
        if result := result.one_or_none():
            (result,) = result
        return result

    async def delete(self, user_id: int, file_id: int) -> bool:
        query = select(UserImage).where(
            UserImage.user_id == user_id, UserImage.image_id == file_id
        )
        result = await self.session.execute(query)
        if result.one_or_none():

            # TODO doesn't work for some reason
            # self.session.delete(result)
            delete_query: "Select" = delete(UserImage).where(
                UserImage.user_id == user_id, UserImage.image_id == file_id
            )
            await self.session.execute(delete_query)
            return True
        else:
            return False

    async def delete_from_api(self, tg_id: int, file_name: str) -> bool:
        user_dal = UserDAL(self.session)
        user = await user_dal.get_by_tg_id(tg_id)

        image_dal = ImageDAL(self.session)
        image = await image_dal.get_by_file_id(file_name)

        return await self.delete(user.id, image.id)

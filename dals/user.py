import logging
import typing

from models import User
from sqlalchemy.future import select
from sqlalchemy.orm import Session, selectinload
from sqlalchemy.sql import Select

logger = logging.getLogger(__name__)


class UserDAL:
    def __init__(self, db_session: Session):
        self.session = db_session

    async def all(self) -> list["User"]:
        query: "Select" = (
            select(User)
            .order_by(User.requests.desc())
            .options(selectinload(User.images))
        )

        result = await self.session.execute(query)
        return result.scalars().all()

    async def create(self, **data) -> "User":
        new_user: "User" = User(**data)
        self.session.add(new_user)
        logger.info(
            f"new user: id {new_user.tg_id} and username: {new_user.username} created!"
        )
        return new_user

    async def get_by_tg_id(self, tg_id: int) -> typing.Union["User", None]:
        query: "Select" = (
            select(User).where(User.tg_id == tg_id).options(selectinload(User.images))
        )

        result = await self.session.execute(query)
        if result := result.one_or_none():
            (result,) = result
        return result

    async def get_by_tg_id_or_create(self, **data) -> "User":
        if user := await self.get_by_tg_id(data.get("tg_id")):
            logger.debug(f"user: {user.tg_id}, {user.username} already exists!")
            return user
        else:
            return await self.create(**data)

    async def increase_requests(self, tg_id: int) -> typing.Optional["User"]:
        user: "User" = await self.get_by_tg_id(tg_id)
        user.requests += 1
        self.session.add(user)
        return user

    async def add_image(self, tg_id: int, image):
        user: "User" = await self.get_by_tg_id(tg_id)
        user.images.append(image)
        return user

import typing

from fastapi import APIRouter, FastAPI
from routers.cat import router as cat_router
from routers.dog import router as dog_router
from routers.image import router as image_router
from routers.user import router as user_router


def create_app() -> FastAPI:
    routers: tuple = (cat_router, dog_router, image_router, user_router)
    app: "FastAPI" = FastAPI(debug=True)
    router = APIRouter()
    [app.include_router(app_router) for app_router in routers]
    app.include_router(router)
    return app

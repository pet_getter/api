import logging
import os

from config import DATABASE_NAME, DATABASE_PORT, DATABASE_USER, DATABASE_USER_PASSWORD
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

logger = logging.getLogger(__name__)

DATABASE_URL = os.environ.get(
    "DATABASE_URL",
    f"postgresql+asyncpg://{DATABASE_USER}:{DATABASE_USER_PASSWORD}@postgres:{DATABASE_PORT}/{DATABASE_NAME}",
)

engine = create_async_engine(DATABASE_URL)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
Base = declarative_base()


async def generate_db() -> None:
    """Generate database"""
    logger.info("Starting to generate database")
    async with engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)
    logger.info("Database generated!")

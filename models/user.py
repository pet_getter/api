import logging

from database.db import Base
from sqlalchemy import Column, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

logger = logging.getLogger(__name__)


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    tg_id = Column(Integer)
    first_name = Column(String)
    username = Column(String)
    last_name = Column(String)
    requests = Column(Integer, default=0)
    images = relationship("Image", secondary="user_image", backref="users")

    __table_args__ = (
        UniqueConstraint("tg_id", name="user_telegram_id"),
        {"extend_existing": True},
    )
    __mapper_args__ = {"eager_defaults": True}

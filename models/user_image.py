import logging

from database.db import Base
from sqlalchemy import Column, ForeignKey, Integer

logger = logging.getLogger(__name__)


class UserImage(Base):
    __tablename__ = "user_image"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    image_id = Column(Integer, ForeignKey("image.id"))

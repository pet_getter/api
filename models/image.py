import logging

from database.db import Base
from sqlalchemy import Column, Integer, String

logger = logging.getLogger(__name__)


class Image(Base):
    __tablename__ = "image"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True, index=True)
    file_id = Column(String, unique=True)
    src = Column(String, unique=True)

    __mapper_args__ = {"eager_defaults": True}
